"""Define Parser for Transacations"""
import csv


class TransactionsTransformer():
    """
    A parser for reading in transactdions
    Input: List of transactions as strings for each row of transaction data
    Example:
    Output: List of transactions as dicts
    """

    def __init__(self, raw_data_string):
        self.raw_data_string = raw_data_string
        self.reader = ""

    def parse(self):
        """
        A parser for reading in transactdions
        Input: List of transactions as strings for each row of transaction data
        Example:
        Output: List of transactions as dicts
        """
        self._read_data()
        self._transform_fieldnames()
        return list(self.reader)

    def parse2(self):
        """
        A parser for reading in transactdions
        Input: List of transactions as strings for each row of transaction data
        Example:
        Output: List of transactions as dicts
        """
        self._read_data()
        self._transform_fieldnames()
        return list(self.reader)

    def _read_data(self):
        self.reader = csv.DictReader(self.raw_data_string)

    def _transform_fieldnames(self):
        self.reader.fieldnames = \
            [fieldname.lower() for fieldname in self.reader.fieldnames]

