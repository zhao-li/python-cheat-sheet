"""Define tests for transaction parser"""
import unittest
from example import TransactionsTransformer


class TransactionsParserTest(unittest.TestCase):
    """Test Parsing Transactions"""

    def test_parsing_transactions(self):
        """test parsing transactions"""

        transaction_fieldnames = \
            '''"","Name","Symbol","Exchange","Open Date","Type","Amount","Open Price","Close Date","Close Price","Gain%","Net P/L"'''  # nopep8 pylint: disable=line-too-long
        first_transaction = \
            '''"","Fastly","FSLY.K","NYSE","01/07/2021","BUY","100.00000000","86.41","01/08/2021","86.42","0.00%","$0.00"'''  # nopep8 pylint: disable=line-too-long
        second_transaction = \
            '''"","Overstockcom","OSTK.O","NASDAQ","01/05/2021","BUY","101.00000000","53.42","01/07/2021","57.16","7.00%","$374.00"'''  # nopep8 pylint: disable=line-too-long

        transactions_data = [
            transaction_fieldnames,
            first_transaction,
            second_transaction,
        ]

        transactions = TransactionsTransformer(transactions_data).parse()

        expected_number_of_transactions = 2
        self.assertEqual(
            len(transactions),
            expected_number_of_transactions
        )

        first_transaction_index = 0
        self.assertEqual(
            transactions[first_transaction_index]["symbol"],
            "FSLY.K"
        )
        self.assertEqual(
            transactions[first_transaction_index]["type"],
            "BUY"
        )


if __name__ == '__main__':
    unittest.main()

